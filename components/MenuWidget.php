<?php
/**
 * Created by PhpStorm.
 * User: kolyh
 * Date: 24.03.2017
 * Time: 12:13
 */

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\Category;

class MenuWidget extends Widget
{
    public $tpl;
    public $model;
    public $data; //тут хранятся все записи категорий из БД
    public $tree; //тут хранится результат работы функции, которая будет строить из обычного массива массив дерева
    public $menuHtml; //будет хранится готовый код html в зависимости от шаблона в tpl

    public function init(){
        parent::init();

        if($this->tpl === null){
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php'; //конкатенируем расширение
    }

    public function run(){
        //получим нужные данные из кэша
        if($this->tpl == 'menu.php'){
            $menu = Yii::$app->cache->get('menu');

            //проверим, есть ли данные в кэше
            if($menu)
                return $menu;
        }

        $this->data = Category::find()->indexBy('id')->asArray()->all();
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);


        //запишем полученные данные в кэш
        if($this->tpl == 'menu.php'){
            Yii::$app->cache->set('menu', $this->menuHtml, 60); //создаем кэш, живущий 1 час
            // debug($this->tree); die;
        }
        return $this->menuHtml;

    }

    //коммент для проверки коммита
    /**
     * функция для формирования из обычного массива массив дерева
     */
    protected function getTree(){
        $tree = [];
        foreach($this->data as $id=>&$node){
            if(!$node['parent_id'])
                $tree[$id] = &$node;
            else
                $this->data[$node['parent_id']]['childs'][$node['id']] = &$node;
        }
        return $tree;

    }


    /**
     * @param $tree - наше дерево сформированное с категориями
     */
    protected function getMenuHtml($tree, $tab = ''){
        $str = '';

        foreach($tree as $category){
            $str .= $this->catToTemplate($category, $tab); //каждый элемент дерева будет передаваться параметром
        }

        return $str;
    }


    /**
     * @param $category - принимает каждый отдельный элемент дерева
     * @return string
     */
    protected function catToTemplate($category, $tab){
        ob_start();
        include __DIR__.'/menu_tpl/'.$this->tpl; //чтобы не производился вывод в браузер,
        //используем функцию буферизации
        return ob_get_clean();
    }
}