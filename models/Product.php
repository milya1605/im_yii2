<?php

namespace app\models;

use yii\db\ActiveRecord;
/**
 *
 */
class Product extends ActiveRecord
{

    //указание, с какой таблицей связываться из БД
    public static function tableName(){
        return 'product';
    }

    /**
     *укажем связь с таблицей product
     */
    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']); //связываемое поле => на какое ссылается
        // в данной таблице
    }

}