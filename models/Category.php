<?php

namespace app\models;

use yii\db\ActiveRecord;
/**
* 
*/
class Category extends ActiveRecord
{
	
	//указание, с какой таблицей связываться из БД
	public static function tableName(){
	    return 'category';
    }

    /**
     *укажем связь с таблицей product
     */
    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']); //связываемое поле => на какое ссылается
        // в данной таблице
    }
	
}